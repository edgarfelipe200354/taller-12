def simular_semiparabolico(Vo, Yo):
    lista_resultados=[]
    
    y=0
    t=0
    g=-9.8
   
    while y>=0:
        x=Vo*t
        y=Yo+(1/2)*(g*t**2)
        t=t+0.2
        lista_resultados.append((x,y))
    return lista_resultados
r=simular_semiparabolico(10,69)

print(r)